# LearnIT Abschlussprojekt
Ein Plattform zum lernen von Programmiersprachen in Eigenstudium auf Quiz Basis

##Inhalt

1. [Das Spiel](#das-spiel)
2. [Getting Started](#getting-started)

<a name="das-spiel"></a>
### Das Spiel

In dem Spiel geht es darum sich in verschiedenen Kategorien über eine Programmiersprache zu informieren und zwr über
Text, Video oder Bilder. Anschliessend gibt es eine kleine Prüfung über den Inhalt des gerade gelernten. Wenn in einem
Level alle Prüfungen bestanden wurden wird das nächste Level freigeschaltet. Jeder registrierte Benutzer hat seinen eigenen
Spielstand.

Hier ein paar Screenshots:

Startseite:
![alt text](readmepics/Startseite.png)

Eingeloggt als neuer User: Test
![alt text](readmepics/UserLoginNewRegistered.png)

Das Spielfeld
![alt text](readmepics/UserJavaNewRegistered.png)

Die Kategorie 'Was ist Java?'
![alt text](readmepics/UserWasIstJavaNewRegistered.png)

Eine Prüfung
![alt text](readmepics/UserWasIstJavaPrüfungNewRegistered.png)

Spielstand nach ein paar bestandenen Prüfungen
![alt text](readmepics/UserPlayedAWhileNewRegistered.png)

<a name="getting-started"></a>
### Getting Started
Um das Projekt zu starten clonen Sie sich folgendes Rpository:

Gitlab - git@gitlab.com:Gerozier/learnit-abschlussprojekt-aw.git

Zuerst brauchen sie eine DB wie z.B. MariaDB und HeidiSQL. In dieser erstellen sie eine neue Datenbank mit dem Namen: 
learnitdb. Diese sollte auf port 3306 laufen. 

In der Datenbank führen sie folgende Abfrage aus:
```
INSERT INTO topic (id, level, name, category_id) VALUES
/* Java Category */
    (1, 1, 'Was ist Java?', 1),
    (2, 1, 'Variablen', 1),
    (3, 1, 'Strings', 1),
    (4, 2, 'if', 1),
    (5, 2, 'for', 1),
    (6, 3, 'while', 1),
    (7, 3, 'Arrays', 1),
    (8, 4, 'OOP', 1),
    (9, 4, 'Spring Boot', 1),
    (10, 5, 'Spring Web', 1),
    (11, 5, 'Maven', 1),
    (12, 6, 'Thymeleaf', 1),
    (13, 6, 'Bootstrap', 1),
    (14, 6, 'Angular', 1),
    (15, 7, 'REST', 1),
    (16, 7, 'Spring Security', 1),
    (17, 8, 'Datenbanken', 1),
    
    /* HTML Category */
    
    (18, 1, 'Basic', 2);
INSERT INTO category (id, name) VALUES
    (1, 'Java'),
     (2, 'HTML - (In Arbeit)');
INSERT INTO module (id, content, level, name, type, topic_id, vblink, link) VALUES
/* Java Category */
    (1, 'Java ist eine objektorientierte Programmiersprache für verschiedene Betriebssysteme und Plattformen
Vorteile:
- Einmal programmieren und überall betreiben
- Eine der meist verbreiteten Sprachen (Quasi-Standard)
- Seit 19learnitdb96 bewährt
- Automatisches Speichermanagement
- Für Desktop-, Web- und Mobilfunkanwendungen', 1, 'Was ist Java?', 'text', 1, '', 'https://de.wikipedia.org/wiki/Java-Technologie'),
    (2, 'Nachdem Start des Programms wird der geschriebene Code vom Compiler in Byte Code umgewandelt und in eine .class Datei geschrieben.
      Dieser wird dann von der Java Virtual Machine für das passende Betriebssystem übersetzt.', 1, 'JVM', 'image', 1, 'JVM.PNG', ''),
    (3, 'Hier ist eine Erklärung zu IDE\'s und ihre Installation.
Angesprochen werden die die beiden wichtigsten Eclips und IntelliJ.
Anbei ein weiterer Link zu IntelliJ Dokumentation, da die hier aufgeführten
Beispiele damit erstellt wurden.', 1, 'IDE', 'video', 1, '', 'https://www.jetbrains.com/idea/documentation/'),
(4, 'byte   1 Byte  ganze Zahlen    -27 bis 27-1 (-128 ... 127)
            short   2 Byte  ganze Zahlen    -215 bis 215-1 (-32768 .. 32767)
            int 4 Byte  ganze Zahlen    -231 bis 231-1 (ca. 2 Mrd.)
            long    8 Byte  ganze Zahlen    -263 bis 263-1
            Fließkommazahlen (Dezimalzahlen, gebrochene Zahlen)
            float   4 Byte  Fließkommazahlen   mit einfacher Genauigkeit
            double  8 Byte  Fließkommazahlen   mit doppelter Genauigkeit
            Wahrheitswerte
            boolean 1 Byte  Wahrheitswerte  true oder false
            Zeichen
            char    2 Byte  Zeichen Unicode',
     1, 'Loops', 'text', 2, '', ''),
(5, 'Casting', 1, 'Strings', 'image', 2, 'Variablen.PNG', ''),
    
    
(6, 'Ein String ist eine Zeichenkette. Der Datentyp String hat als Wertebereich die Menge aller Zeichen­ketten. Strings werden in Anführungs­zeichen gesetzt; 
Beispiele für Strings sind "abba", "Hallo Fritz!", "@%#&$", "x" sowie auch "". Die Länge eines Strings ist die Anzahl der Zeichen, aus denen er besteht. 
So hat beispiels­weise der String "abba" die Länge 4. Der leere String "" hat die Länge 0, da er 0 Zeichen enthält. Die einzelnen Zeichen eines Strings der Länge n werden von 0 bis n-1 nummeriert.
Das folgende Programm­stück zeigt die Deklaration einer Variablen vom Typ String gefolgt von einer Wert­zuweisung und anschließender Ausgabe des Strings.
String s;
s="Hallo!";
System.out.println(s);',
 1, 'Was sind Strings', 'text', 3, '', ''),
(7, 'Der Datentyp String ist in Java kein einfacher Datentyp wie etwa int oder double oder boolean, sondern eine Klasse. Eine Variable vom Typ String enthält daher nicht den String selbst, 
    sondern sie enthält einen Verweis auf ein Objekt der Klasse String.
Die wichtigste Konsequenz aus dieser Tatsache ist, dass Strings nicht mit dem Vergleichs­operator == verglichen werden können, denn der Vergleichs­operator prüft, ob die Verweise gleich 
sind und nicht, ob die Strings gleich sind, auf die verwiesen wird. Für den Vergleich von Strings steht die Methode equals zur Verfügung (s.u.).
Obwohl Strings eigentlich Objekte sind, sieht die Wert­zuweisung syntaktisch genauso aus wie bei den einfachen Datentypen. Tatsächlich aber wird mit der Anweisung',
 1, 'Strings Datentyp', 'text', 3, '', ''),
 
 
 
(8, 'Ein String ist eine Zeichenkette. Der Datentyp String hat als Wertebereich die Menge aller Zeichen­ketten. Strings werden in Anführungs­zeichen gesetzt; 
Beispiele für Strings sind "abba", "Hallo Fritz!", "@%#&$", "x" sowie auch "". Die Länge eines Strings ist die Anzahl der Zeichen, aus denen er besteht. 
So hat beispiels­weise der String "abba" die Länge 4. Der leere String "" hat die Länge 0, da er 0 Zeichen enthält. Die einzelnen Zeichen eines Strings der Länge n werden von 0 bis n-1 nummeriert.
Das folgende Programm­stück zeigt die Deklaration einer Variablen vom Typ String gefolgt von einer Wert­zuweisung und anschließender Ausgabe des Strings.
String s;
s="Hallo!";
System.out.println(s);',
 1, 'Was sind Strings', 'text', 3, '', ''),
 
(9, 'Die If-Anweisung dient dazu einen Ausdruck auszuwerten und je nach Ergebnis weiterzuverfahren. Der Ausdruck muss dabei einen Wert vom Datentyp boolean haben. 
    Um If-Anweisungen wird man im kaum einen Programm herumkommen. Das simple Konzept ist sehr mächtig und kann in den unterschiedlichsten Situationen eingesetzt werden.',
 2, 'If-Anweisungen', 'text', 4, '', ''),
 
 
 (10, 'HTML-Tags sind Auszeichner, die durch < und > eingeschlossen werden.
        <Tag>Inhalt...</Tag>',
 2, 'HTML-Tags', 'text', 18, '', '');
    
    
    
INSERT INTO `learnitdb`.`user_topic_done` (`user_id`, `topic_id`) VALUES 
    ('1', '1'),
    ('1', '2'),
    ('1', '3'),
    ('1', '4'),
    ('1', '5'),
    ('1', '7');
INSERT INTO `learnitdb`.`user_topic_active` (`user_id`, `topic_id`) VALUES 
    ('1', '8'),
    ('1', '9'),
    ('1', '6');
INSERT INTO `learnitdb`.`question` (`id`, `content`, `topic_id`) VALUES
/*Java Category */
('1', 'Auf welchem Betriebssystem läuft Java?', '1'),
('2', 'Wie weit ist Java verbreitet?', '1'),
('3', 'In welcher Datei schreiben wir unseren Java Code?', '1'),
('4', 'Was bedeutet JVM?', '1'),
('5', 'Wozu werden IDE\'s beim Programmieren benutzt?', '1'),
('6', 'Was ist kein Datentyp in Java?', '2'),
('7', 'Was bedeutet das Casting von Variablen?', '2'),
('8', 'Was sind Strings in Java?', '3'),
('9', 'Was ist keine Methode der String Klasse?', '3'),
('10', 'Was ist gültiger Syntax in Java?', '4'),
/*HTML Category */
('11', 'Was ist kein HTML-Tag?', '18');
INSERT INTO `learnitdb`.`answer` (`id`, `content`,`correct`, `question_id`) VALUES
/*Java Category */
('1', 'Windows', 0, '1'),
('2', 'Mac', 0, '1'),
('3', 'Linux', 0, '1'),
('4', 'Plattformunabhängig', 1, '1'),
('5', 'Europa', 0, '2'),
('6', 'Asien', 0, '2'),
('7', 'Weltweit', 1, '2'),
('8', 'Amerika', 0, '2'),
('9', '.java', 1, '3'),
('10', '.class', 0, '3'),
('11', '.txt', 0, '3'),
('12', '.html', 0, '3'),
('13', 'Java Visual Magic', 0, '4'),
('14', 'Java Vision Managment', 0, '4'),
('15', 'Java Virtual Machine', 1, '4'),
('16', 'Java Vacation Mode', 1, '4'),
('17', 'Spielen', 0, '5'),
('18', 'Als Browser', 0, '5'),
('19', 'Code schreiben', 1, '5'),
('20', 'Bilder malen', 0, '5'),
('21', 'byte', 0, '6'),
('22', 'single', 1, '6'),
('23', 'double', 0, '6'),
('24', 'char', 0, '6'),
('25', 'Variablen auswählen', 0, '7'),
('26', 'Umwandeln von Variablen in einen anderen Typ', 1, '7'),
('27', 'Variablen löschen', 0, '7'),
('28', 'Eckige Klammern vor Variablen schreiben', 0, '7'),
('29', 'Arrays von Chars', 1, '8'),
('30', 'Datentyp zum Speichern von sehr großen Zahlen', 0, '8'),
('31', 'Unterwäsche', 0, '8'),
('32', 'Schnüre', 0, '8'),
('33', '.trim()', 0, '9'),
('34', '.length()', 0, '9'),
('35', '.replace()', 0, '9'),
('36', '.cut()', 1, '9'),
('37', 'if foo then', 0, '10'),
('38', 'if (foo) { 
            else   }', 0, '10'),
('39', 'if (foo) {}
            else {}', 1, '10'),
('40', 'do if (foo)', 0, '10'),
/*HTML Category */
('41', '<div>', 0, '11'),
('42', '<span>', 0, '11'),
('43', '<break>', 1, '11'),
('44', '<p>', 0, '11');
```

Damit ist die Datenbank befüllt. Anschliessend öffnen sie in ihrer IDE separat aus dem Projektordner einmal frontend und
backend. Das frontend wird mit npm install installiert und mit ng serve gestartet. Im Backend wird einfach die 
Application gestartet. Nun können sie auf: http://localhost:4206/ das Spiel beginnen.


