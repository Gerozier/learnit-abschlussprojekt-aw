import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionUserComponent } from './session-user/session-user.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './_template/navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatRadioModule, MatSelectModule} from '@angular/material';
import { DemoComponent } from './_template/demo/demo.component';
import { Demo2Component } from './_template/demo2/demo2.component';
import { CarousselComponent } from './_template/caroussel/caroussel.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RegistryComponent } from './registration/registry.component';
import { ImpressumComponent } from './_template/impressum/impressum.component';
import { TopicComponent } from './_template/topic/topic.component';
import { CategoryComponent } from './_template/category/category.component';
import { ModuleComponent } from './_template/module/module.component';
import {ContentComponent} from './_template/content/content.component';
import { ExamComponent } from './_template/exam/exam.component';

@NgModule({
  declarations: [
    AppComponent,
    SessionUserComponent,
    LoginComponent,
    NavbarComponent,
    DemoComponent,
    Demo2Component,
    CarousselComponent,
    RegistryComponent,
    ImpressumComponent,
    TopicComponent,
    CategoryComponent,
    ModuleComponent,
    ContentComponent,
    ExamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    LayoutModule,
    NgbModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCardModule,
    MatRadioModule,
    MatCheckboxModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
