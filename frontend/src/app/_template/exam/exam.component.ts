import { Component, OnInit } from '@angular/core';
import {User} from '../../_interface/user';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../../security.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Question} from '../../_interface/question';
import {Answer} from '../../_interface/answer';
import {Module} from '../../_interface/module';
import {Topic} from '../../_interface/topic';

@Component({
  selector: 'app-question',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {
  sessionUser: User|null;
  questions: Question[] = [];
  comAnswer: Answer[] = [];
  topic: Topic;
  private done: boolean;
  numOfWrong = 0;

  constructor(private http: HttpClient, private securityService: SecurityService,
              private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );

    const id = this.route.snapshot.paramMap.get('id');
    this.http.get<Question[]>('/api/question/' + id).subscribe(
      questions => {
        this.questions = questions;
      }
    );
    this.http.get<Topic>('/api/singletopic/'.concat(id)).subscribe(
      topic => {
        this.topic = topic;
      }
    );
  }
  checkAnswers() {
    this.done = false;
    for (const content of this.comAnswer) {
      if (!content.correct) {
        this.numOfWrong ++;
      }
    }
    if (this.numOfWrong < this.comAnswer.length / 5) {
      this.done = true;
    }
    if (this.done) {
      this.setTopicDone();
    } else {
      alert('Leider nicht bestanden.\nVersuch es doch noch einmal.');
    }
    this.router.navigate(['/topic/' + this.topic.category.id]);
    this.numOfWrong = 0;
  }

  setTopicDone() {
    const id = this.route.snapshot.paramMap.get('id');
    this.http.get<Module>('/api/done/module/' + id).subscribe();
  }
}
