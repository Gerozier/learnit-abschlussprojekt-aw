import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../../security.service';
import {User} from '../../_interface/user';
import {Category} from '../../_interface/category';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  private sessionUser: User|null = null;
  private categories: Category[] = [];

  img = ['./assets/imagesandgifs/micha.png', './assets/imagesandgifs/michasad.gif', './assets/imagesandgifs/michahappy.gif'];

  constructor(private securityService: SecurityService, private http: HttpClient) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    // console.log(this.sessionUser.userName);

    this.http.get<Category[]>('/api/category').subscribe(
      categories => this.categories = categories);


  }

}
