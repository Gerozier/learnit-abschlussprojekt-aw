import { Component, OnInit } from '@angular/core';
import {User} from '../../_interface/user';
import {Module} from '../../_interface/module';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../../security.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  sessionUser: User | null;
  module: Module;


  constructor(private http: HttpClient, private securityService: SecurityService,
              private route: ActivatedRoute, private location: Location) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );

    const id = this.route.snapshot.paramMap.get('id');
    this.http.get<Module>('/api/module/' + id).subscribe(
      module => this.module = module
    );


  }

  backClick() {
    this.location.back();
  }


}
