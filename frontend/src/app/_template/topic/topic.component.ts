import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Topic} from '../../_interface/topic';
import {User} from '../../_interface/user';
import {SecurityService} from '../../security.service';
import {TopicDto} from '../../_interface/topic-dto';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.css']
})
export class TopicComponent implements OnInit {
  sessionUser: User|null;
  topic: Topic;
  topics: TopicDto = { active: [], done: [], nonactive: []};
  levels: number[] = [];
  maxlvl = 0;


  constructor(private http: HttpClient, private securityService: SecurityService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );

    const id = this.route.snapshot.paramMap.get('id');
    this.http.get<TopicDto>('/api/topic/' + id).subscribe(
      topics => {
        this.topics = topics;
        for (this.topic of this.topics.nonactive) {
          if (this.topic.level >= this.maxlvl) {
            this.maxlvl = this.topic.level;
          }
        }
        for (this.topic of this.topics.active) {
          if (this.topic.level >= this.maxlvl) {
            this.maxlvl = this.topic.level;
          }
        }
        for (let i = 1 ; i <= this.maxlvl; i++) {
          this.levels.push(i);
        }
      }
    );
  }
}
