import { Component } from '@angular/core';

@Component({
  selector: 'app-caroussel',
  templateUrl: './caroussel.component.html',
  styleUrls: ['./caroussel.component.css']
})


export class CarousselComponent {

  img = ['./assets/imagesandgifs/micha.png', './assets/imagesandgifs/michasad.gif', './assets/imagesandgifs/michahappy.gif'];
}
