import { Component, OnInit } from '@angular/core';
import {User} from '../../_interface/user';
import {TopicDto} from '../../_interface/topic-dto';
import {HttpClient} from '@angular/common/http';
import {SecurityService} from '../../security.service';
import {ModuleDTO} from '../../_interface/module-dto';
import {Module} from '../../_interface/module';
import {ActivatedRoute} from '@angular/router';
import {Question} from '../../_interface/question';
import {Topic} from '../../_interface/topic';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})
export class ModuleComponent implements OnInit {
  sessionUser: User | null;
  module: Module;
  modules: ModuleDTO = {active: [], done: [], nonactive: []};
  topic: Topic;
  id: string;


  constructor(private http: HttpClient, private securityService: SecurityService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    // Topic id
    this.id = this.route.snapshot.paramMap.get('id');
    // Get Modules belonging to Topic with this id
    this.http.get<ModuleDTO>('/api/modules/' + this.id).subscribe(
      modules => this.modules = modules);
    // To control for question existence, get Topic with this id
    this.http.get<Topic>('api/singletopic/' + this.id).subscribe(
      topic => {
        this.topic = topic;
        // console.log(this.topic.id);
        // console.log(this.topic.questions[0].content);
        // console.log(this.topic.questions.length);
      }
    );
  }
}
