import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RegistrationDTO} from '../_interface/registration';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.css']
})
export class RegistryComponent implements OnInit {
  path: string;

  newUserDTO: RegistrationDTO = {
    username: '',
    email: '',
    password1: '',
    password2: ''
  };


  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  register() {

    if (this.newUserDTO.password1 === this.newUserDTO.password2) {
      this.path = '/';
      this.http.post<RegistrationDTO>('/api/registry', this.newUserDTO).subscribe(registration => this.newUserDTO = registration);
      this.newUserDTO = {
        username: '',
        email: '',
        password1: '',
        password2: ''
      };
    } else {
      this.path = '#';
      alert('Passwörter stimmen nicht überein');
      this.newUserDTO = {
        username: this.newUserDTO.username,
        email: this.newUserDTO.email,
        password1: '',
        password2: ''
      };
    }
  }

}
