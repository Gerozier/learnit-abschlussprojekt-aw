import {Topic} from './topic';

export interface TopicDto {
  active: Topic[];
  done: Topic[];
  nonactive: Topic[];
}
