import { Topic } from './topic';

export interface Module {
  id: number;
  name: string;
  type: string;
  content: string;
  vblink: string;
  link: string;
  topic: Topic;
}
