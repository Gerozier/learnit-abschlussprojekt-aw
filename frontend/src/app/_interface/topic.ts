import {Category} from './category';

import {Question} from './question';

export interface Topic {
  id: number;
  name: string;
  level: number;
  category: Category;
  questions: Question[];
}
