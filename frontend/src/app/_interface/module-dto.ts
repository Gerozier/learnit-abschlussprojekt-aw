import {Module} from './module';


export interface ModuleDTO {
  active: Module[];
  done: Module[];
  nonactive: Module[];
}
