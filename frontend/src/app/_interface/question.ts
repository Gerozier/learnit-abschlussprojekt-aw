import {Answer} from './answer';
import {Topic} from './topic';

export interface Question {
  id: number;
  content: string;
  answers: Answer[];
  topic: Topic;
}
