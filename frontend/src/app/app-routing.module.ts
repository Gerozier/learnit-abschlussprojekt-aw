import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {DemoComponent} from './_template/demo/demo.component';
import {Demo2Component} from './_template/demo2/demo2.component';
import {CarousselComponent} from './_template/caroussel/caroussel.component';
import {RegistryComponent} from './registration/registry.component';
import {ImpressumComponent} from './_template/impressum/impressum.component';
import {TopicComponent} from './_template/topic/topic.component';
import {ModuleComponent} from './_template/module/module.component';
import {ContentComponent} from './_template/content/content.component';
import {ExamComponent} from './_template/exam/exam.component';


const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'module/:id', component: ModuleComponent},
  {path: 'modules/:id', component: ModuleComponent},
  {path: 'topic/:id', component: TopicComponent},
  {path: 'demo', component: DemoComponent},
  {path: 'demovar', component: Demo2Component},
  {path: 'gettingstarted', component: CarousselComponent},
  {path: 'registry', component: RegistryComponent},
  {path: 'impressum', component: ImpressumComponent},
  {path: 'content/:id', component: ContentComponent},
  {path: 'exam/:id', component: ExamComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
