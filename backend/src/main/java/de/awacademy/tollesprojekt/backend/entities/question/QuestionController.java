package de.awacademy.tollesprojekt.backend.entities.question;

import de.awacademy.tollesprojekt.backend.entities.topic.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class QuestionController {

    private QuestionRepository questionRepository;
    private TopicRepository topicRepository;

    @Autowired
    public QuestionController(QuestionRepository questionRepository, TopicRepository topicRepository) {
        this.questionRepository = questionRepository;
        this.topicRepository = topicRepository;
    }

    @GetMapping("/api/question/{id}")
    public List<Question> getQuestion(@PathVariable Long id) {
//        getTopic for id
//        get question for topic
        List<Question> questionList = topicRepository.findById(id).get().getQuestions();
        return questionList;
    }
}
