package de.awacademy.tollesprojekt.backend.security;

import de.awacademy.tollesprojekt.backend.entities.user.User;
import de.awacademy.tollesprojekt.backend.entities.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecurityService implements UserDetailsService {
    private UserService userService;
    @Autowired
    public SecurityService(UserService userService) {
        this.userService = userService;
    }
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userService.findByUsernameIgnoreCase(username);
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), List.of());
    }
}
