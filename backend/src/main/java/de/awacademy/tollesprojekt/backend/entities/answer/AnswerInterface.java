package de.awacademy.tollesprojekt.backend.entities.answer;

import org.springframework.data.repository.CrudRepository;

public interface AnswerInterface extends CrudRepository<Answer, Long> {
}
