package de.awacademy.tollesprojekt.backend.entities.module;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.awacademy.tollesprojekt.backend.entities.topic.Topic;
import de.awacademy.tollesprojekt.backend.entities.user.User;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Module {

    // Fields

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Lob
    private String content;

    private String type;

    private Long level;

    private String vblink;

    private String link;

    @JsonIgnore
    @ManyToMany(mappedBy = "modulesActive")
    private List<User> usersActive = new LinkedList<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "modulesDone")
    private List<User> usersDone = new LinkedList<>();

    @JsonIgnore
    @ManyToOne
    private Topic topic;

    // Constructor

    public Module() {
    }

    public Module(String name, String content, String type, Long level, String vblink, String link) {
        this.name = name;
        this.content = content;
        this.type = type;
        this.level = level;
        this.vblink = vblink;
        this.link = link;
    }

    // Getter/Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public List<User> getUsersActive() {
        return usersActive;
    }

    public void setUsersActive(List<User> usersActive) {
        this.usersActive = usersActive;
    }

    public void addUserActive(User user) {
        usersActive.add(user);
    }

    public List<User> getUsersDone() {
        return usersDone;
    }

    public void setUsersDone(List<User> usersDone) {
        this.usersDone = usersDone;
    }

    public void addUserDone(User user) {
        usersDone.add(user);
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String getVblink() {
        return vblink;
    }

    public void setVblink(String vblink) {
        this.vblink = vblink;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
