package de.awacademy.tollesprojekt.backend;



import de.awacademy.tollesprojekt.backend.entities.user.User;
import de.awacademy.tollesprojekt.backend.entities.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class SetupComponent {


    private final UserService userService;

    @Autowired
    public SetupComponent(UserService userService) {
        this.userService = userService;
    }

    @EventListener
    @Transactional
    public void handleApplicationReady(ApplicationReadyEvent event) {
        if (!userService.usernameExists("Dennis")) {
            userService.register("Dennis", "Dennis", "123");
        }
        if (!userService.usernameExists("Leon")) {
            userService.register("Leon", "Leon", "123");
        }
        if (!userService.usernameExists("Jan")) {
            userService.register("Jan", "Jan", "123");
        }
    }
}
