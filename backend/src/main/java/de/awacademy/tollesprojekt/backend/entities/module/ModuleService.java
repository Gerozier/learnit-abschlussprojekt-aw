package de.awacademy.tollesprojekt.backend.entities.module;

import org.springframework.beans.factory.annotation.Autowired;

public class ModuleService {

    // Fields

    private ModuleRepository moduleRepository;

    // Constructor

    @Autowired
    public ModuleService(ModuleRepository moduleRepository) {
        this.moduleRepository = moduleRepository;
    }
}
