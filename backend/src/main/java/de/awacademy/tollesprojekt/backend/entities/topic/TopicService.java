package de.awacademy.tollesprojekt.backend.entities.topic;

import de.awacademy.tollesprojekt.backend.entities.category.CategoryRepository;
import de.awacademy.tollesprojekt.backend.entities.module.ModuleRepository;
import de.awacademy.tollesprojekt.backend.entities.user.UserRepository;
import de.awacademy.tollesprojekt.backend.entities.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TopicService {

    // Fields

    private TopicRepository topicRepository;
    private UserRepository userRepository;
    private CategoryRepository categoryRepository;
    private ModuleRepository moduleRepository;
    private UserService userService;

    // Constructor

    @Autowired
    public TopicService(TopicRepository topicRepository, UserRepository userRepository, ModuleRepository moduleRepository, UserService userService, CategoryRepository categoryRepository) {
        this.topicRepository = topicRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.moduleRepository = moduleRepository;
        this.userService = userService;
    }

    public TopicService() {
    }
    // Methods

    public List<Topic> getAllTopicsOfCat(Long id) {
        return (List<Topic>) topicRepository.findAll();
    }

    public Long getMaxLvlOfTopicList(List<Topic> topics){
        Long maxVal = Long.valueOf(0);
        for (Topic topic : topics) {
            if (maxVal<topic.getLevel());
            maxVal=topic.getLevel();
        }
        return maxVal;
    }
}
