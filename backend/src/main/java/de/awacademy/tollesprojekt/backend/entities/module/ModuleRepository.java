package de.awacademy.tollesprojekt.backend.entities.module;

import de.awacademy.tollesprojekt.backend.entities.topic.Topic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModuleRepository extends CrudRepository<Module, Long> {
    List<Module> findAllByTopic(Topic topic);
}
