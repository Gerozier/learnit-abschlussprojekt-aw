package de.awacademy.tollesprojekt.backend.security;

public class LoginDTO {

    // Fields

    private String username;
    private String password;

    // Constructor

    public LoginDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    // Getter/Setter

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
