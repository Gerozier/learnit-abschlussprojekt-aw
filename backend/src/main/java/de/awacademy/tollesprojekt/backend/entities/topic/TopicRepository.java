package de.awacademy.tollesprojekt.backend.entities.topic;

import de.awacademy.tollesprojekt.backend.entities.category.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TopicRepository extends CrudRepository<Topic, Long> {
    List<Topic> findAllByCategory(Category category);
    List<Topic> findAllByLevelAndCategory(Long lvl, Category category);
    Optional<Topic> findById(Long id);
}
