package de.awacademy.tollesprojekt.backend.entities.topic;

import java.util.List;

public class TopicDTO {

    // Fields

    private List<Topic> active;
    private List<Topic> done;
    private List<Topic> nonactive;

    // Constructor

    public TopicDTO(List<Topic> active, List<Topic> done, List<Topic> nonactive) {
        this.active = active;
        this.done = done;
        this.nonactive = nonactive;
    }

    // Getter/Setter

    public List<Topic> getActive() {
        return active;
    }

    public void setActive(List<Topic> active) {
        this.active = active;
    }

    public List<Topic> getDone() {
        return done;
    }

    public void setDone(List<Topic> done) {
        this.done = done;
    }

    public List<Topic> getNonactive() {
        return nonactive;
    }

    public void setNonactive(List<Topic> nonactive) {
        this.nonactive = nonactive;
    }
}
