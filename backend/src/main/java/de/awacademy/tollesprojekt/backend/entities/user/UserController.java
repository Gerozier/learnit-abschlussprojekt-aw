package de.awacademy.tollesprojekt.backend.entities.user;

import de.awacademy.tollesprojekt.backend.entities.module.Module;
import de.awacademy.tollesprojekt.backend.entities.module.ModuleRepository;
import de.awacademy.tollesprojekt.backend.entities.topic.Topic;
import de.awacademy.tollesprojekt.backend.entities.topic.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    // Fields

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private TopicRepository topicRepository;
    private ModuleRepository moduleRepository;
    private UserService userService;
    // Constructor

    @Autowired
    public UserController(UserRepository userRepository, PasswordEncoder passwordEncoder, TopicRepository topicRepository, ModuleRepository moduleRepository, UserService userService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.topicRepository = topicRepository;
        this.moduleRepository = moduleRepository;
        this.userService = userService;
    }


    // Methods

    @PostMapping("/api/registry")
    public void registerUser(@RequestBody RegistrationDTO registrationDTO) {
        String encPassword = passwordEncoder.encode(registrationDTO.getPassword1());
        User user = new User(registrationDTO.getUserName(),registrationDTO.getEmail(),encPassword);
        userService.fillTopicsAndModules(user, topicRepository, moduleRepository);
        userRepository.save(user);
    }
}
