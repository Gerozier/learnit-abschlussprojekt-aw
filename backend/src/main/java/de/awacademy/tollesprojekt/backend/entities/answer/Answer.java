package de.awacademy.tollesprojekt.backend.entities.answer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.awacademy.tollesprojekt.backend.entities.question.Question;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Answer {

    // Fields

    @Id
    @GeneratedValue
    private Long id;

    private String content;

    private boolean correct;

    @JsonIgnore
    @ManyToOne
    private Question question;

    // Constructor

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
