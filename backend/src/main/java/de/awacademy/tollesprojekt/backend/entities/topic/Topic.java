package de.awacademy.tollesprojekt.backend.entities.topic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.awacademy.tollesprojekt.backend.entities.category.Category;
import de.awacademy.tollesprojekt.backend.entities.question.Question;
import de.awacademy.tollesprojekt.backend.entities.user.User;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Topic<Questions> {

    // Fields

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private Long level;

    @JsonIgnore
    @ManyToMany(mappedBy = "topicsActive")
    private List<User> usersActive = new LinkedList<>();

    @JsonIgnore
    @ManyToMany(mappedBy = "topicsDone")
    private List<User> usersDone = new LinkedList<>();

    @ManyToOne
    private Category category;

    @OneToMany(mappedBy = "topic")
    private List<Question> questions;

    // Constructor

    public Topic() {}

    public Topic(String name, Long level) {
        this.name = name;
        this.level = level;
    }

    public Topic(String name, Category category, Long level) {
        this.name = name;
        this.category = category;
        this.level = level;
    }

    // Getter/Setter

    public void addUserActive(User user) {
        usersActive.add(user);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsersActive() {
        return usersActive;
    }

    public void setUsersActive(List<User> usersActive) {
        this.usersActive = usersActive;
    }

    public List<User> getUsersDone() {
        return usersDone;
    }

    public void setUsersDone(List<User> usersDone) {
        this.usersDone = usersDone;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
