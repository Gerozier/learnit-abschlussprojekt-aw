package de.awacademy.tollesprojekt.backend.entities.user;

import javax.validation.constraints.NotEmpty;

public class RegistrationDTO {

    // Fields

    @NotEmpty
    private String username;
    private String email;
    private String password1;
    private String password2;

    // Constructor

    public RegistrationDTO(@NotEmpty String username, String email, String password1, String password2) {
        this.username = username;
        this.email = email;
        this.password1 = password1;
        this.password2 = password2;
    }

    // Getter/Setter

    public String getUserName() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword1() {
        return password1;
    }

    public String getPassword2() {
        return password2;
    }
}
