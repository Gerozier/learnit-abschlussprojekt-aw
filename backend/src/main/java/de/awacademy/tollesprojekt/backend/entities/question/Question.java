package de.awacademy.tollesprojekt.backend.entities.question;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.awacademy.tollesprojekt.backend.entities.answer.Answer;
import de.awacademy.tollesprojekt.backend.entities.topic.Topic;

import javax.persistence.*;
import java.util.List;

@Entity
public class Question {

    // Fields

    @Id
    @GeneratedValue
    private Long id;

    private String content;

    @ManyToOne
    private Topic topic;


    @OneToMany(mappedBy = "question")
    private List<Answer> answers;

    // Getter/Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
