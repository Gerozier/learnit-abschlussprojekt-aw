package de.awacademy.tollesprojekt.backend.entities.module;

import java.util.List;

public class ModuleDTO {

    // Fields

    private List<Module> active;
    private List<Module> done;
    private List<Module> nonactive;

    // Constructor

    public ModuleDTO(List<Module> active, List<Module> done, List<Module> nonactive) {
        this.active = active;
        this.done = done;
        this.nonactive = nonactive;
    }

    // Getter/Setter

    public List<Module> getActive() {
        return active;
    }

    public void setActive(List<Module> active) {
        this.active = active;
    }

    public List<Module> getDone() {
        return done;
    }

    public void setDone(List<Module> done) {
        this.done = done;
    }

    public List<Module> getNonactive() {
        return nonactive;
    }

    public void setNonactive(List<Module> nonactive) {
        this.nonactive = nonactive;
    }
}


