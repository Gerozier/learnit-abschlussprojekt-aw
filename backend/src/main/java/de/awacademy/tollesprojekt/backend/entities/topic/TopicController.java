package de.awacademy.tollesprojekt.backend.entities.topic;

import de.awacademy.tollesprojekt.backend.entities.category.CategoryRepository;
import de.awacademy.tollesprojekt.backend.entities.module.ModuleRepository;
import de.awacademy.tollesprojekt.backend.entities.user.User;
import de.awacademy.tollesprojekt.backend.entities.user.UserRepository;
import de.awacademy.tollesprojekt.backend.entities.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
public class TopicController {

    // Fields

    private TopicRepository topicRepository;
    private UserRepository userRepository;
    private CategoryRepository categoryRepository;
    private ModuleRepository moduleRepository;
    private UserService userService;

    // Constructor

    @Autowired
    public TopicController(TopicRepository topicRepository, UserRepository userRepository, ModuleRepository moduleRepository, UserService userService, CategoryRepository categoryRepository) {
        this.topicRepository = topicRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.moduleRepository = moduleRepository;
        this.userService = userService;
    }

    // Methods

    @GetMapping("/api/topic/{id}")
    public TopicDTO getTopicLists(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
        User newUser = userRepository.findByUsernameIgnoreCase(user.getUsername()).get();

        userService.fillTopicsAndModules(newUser, topicRepository, moduleRepository);

        List<Topic> activeTopics = newUser.getTopicsActiveForCat(id);
        List<Topic> doneTopics = newUser.getTopicsDoneForCat(id);

        activeTopics.removeAll(doneTopics);
        List<Topic> nonActiveTopics = new LinkedList<>();

        for (Topic topic : topicRepository.findAllByCategory(categoryRepository.findCategoryById(id).get())) {
            if (!activeTopics.contains(topic) && !doneTopics.contains(topic)) {
                nonActiveTopics.add(topic);
            }
        }


        TopicDTO rdyTopicDTO = new TopicDTO(activeTopics, doneTopics, nonActiveTopics);
        activeTopics.removeAll(doneTopics);

        return rdyTopicDTO;
    }

    @GetMapping("/api/singletopic/{id}")
    public Topic getTopic(@PathVariable Long id) {
        Topic topic = topicRepository.findById(id).get();
        return topic;
    }

}
