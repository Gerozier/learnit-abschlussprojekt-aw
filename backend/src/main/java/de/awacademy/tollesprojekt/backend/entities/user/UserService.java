package de.awacademy.tollesprojekt.backend.entities.user;

import de.awacademy.tollesprojekt.backend.entities.category.Category;
import de.awacademy.tollesprojekt.backend.entities.category.CategoryRepository;
import de.awacademy.tollesprojekt.backend.entities.module.Module;
import de.awacademy.tollesprojekt.backend.entities.module.ModuleRepository;
import de.awacademy.tollesprojekt.backend.entities.topic.Topic;
import de.awacademy.tollesprojekt.backend.entities.topic.TopicRepository;
import de.awacademy.tollesprojekt.backend.entities.topic.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    // Fields

    private final UserRepository userRepository;
    private final TopicRepository topicRepository;
    private final ModuleRepository moduleRepository;
    private final PasswordEncoder passwordEncoder;
    private final CategoryRepository categoryRepository;
    private TopicService topicService;

    @Autowired
    public UserService(UserRepository userRepository, TopicRepository topicRepository, ModuleRepository moduleRepository, PasswordEncoder passwordEncoder, CategoryRepository categoryRepository) {
        this.userRepository = userRepository;
        this.topicRepository = topicRepository;
        this.moduleRepository = moduleRepository;
        this.passwordEncoder = passwordEncoder;
        this.categoryRepository = categoryRepository;
    }

    // Constructor



   // Methods

    public boolean usernameExists(String username) {
        return userRepository.existsByUsernameIgnoreCase(username);
    }

    public User findByUsernameIgnoreCase(String username) {
        User user = userRepository.findByUsernameIgnoreCase(username)
                .orElseThrow(() -> new UsernameNotFoundException("Nicht gefunden"));
        return user;
    }

    public Optional<User> findById(long id) {
        return userRepository.findById(id);
    }

    public void register(String username, String email, String password) {
        String encPassword = passwordEncoder.encode(password);
        User user = new User(username,email,encPassword);
        userRepository.save(new User(username,email,encPassword));

        fillTopicsAndModules(user, topicRepository, moduleRepository);


    }

    public void fillTopicsAndModules(User user, TopicRepository topicRepository, ModuleRepository moduleRepository) {
        topicService = new TopicService();
        Long currLvl;
        for (Topic topic: topicRepository.findAll()) {
            if (!user.getTopicsActive().contains(topic)){
                if (topic.getLevel() == 1) {
                    user.addTopicActive(topic);
                    topic.addUserActive(user);
                    topicRepository.save(topic);

                }
            }
        }
        for (Module module: moduleRepository.findAll()) {
            if (!user.getModulesActive().contains(module)) {
                if (module.getLevel() == 1) {
                    user.addModuleActive(module);
                    module.addUserActive(user);
                }
                moduleRepository.save(module);
            }
        }
        for (Category category : categoryRepository.findAll() ) {
            for (int i = 1; i < topicService.getMaxLvlOfTopicList(category.getTopics()); i++){
                currLvl = Long.valueOf(i);
                if(getDoneListByLevel(user,currLvl,category.getId()).equals(topicRepository.findAllByLevelAndCategory(currLvl,category))){
                    for (Topic topic  : topicRepository.findAllByCategory(category)) {
                        if(topic.getLevel()==currLvl+1){
                            if(!user.getTopicsActive().contains(topic)) {
                                user.addTopicActive(topic);
                                topic.addUserActive(user);
                                topicRepository.save(topic);
                                userRepository.save(user);
                            }
                        }
                    }
                }
            }

        }
    }
    public List<Topic> getDoneListByLevel(User user, Long level, Long catId){

        List<Topic> lvlList = new LinkedList<>();
        for (Topic topic : topicRepository.findAllByLevelAndCategory(level,categoryRepository.findCategoryById(catId).get())) {
            if(user.getTopicsDone().contains(topic))
                lvlList.add(topic);
        }

        return lvlList;
    }
}




