package de.awacademy.tollesprojekt.backend.entities.module;


import de.awacademy.tollesprojekt.backend.entities.topic.Topic;
import de.awacademy.tollesprojekt.backend.entities.topic.TopicRepository;
import de.awacademy.tollesprojekt.backend.entities.user.User;
import de.awacademy.tollesprojekt.backend.entities.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
public class ModuleController {

    // Fields

    private ModuleRepository moduleRepository;
    private UserRepository userRepository;
    private TopicRepository topicRepository;

    // Constructor

    @Autowired
    public ModuleController(ModuleRepository moduleRepository, UserRepository userRepository,
                            TopicRepository topicRepository) {
        this.moduleRepository = moduleRepository;
        this.userRepository = userRepository;
        this.topicRepository = topicRepository;
    }

    // Methods

    @GetMapping("/api/modules/{id}")
    public ModuleDTO getModuleLists(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
        User newUser = userRepository.findByUsernameIgnoreCase(user.getUsername()).get();

        List<Module> activeModules = newUser.getModulesActiveForTopic(id);
        List<Module> doneModules = newUser.getModulesDoneForTopic(id);

        activeModules.removeAll(doneModules);
        List<Module> nonActiveModules = new LinkedList<>();

        for (Module module : moduleRepository.findAllByTopic(topicRepository.findById(id).get())) {
            if(!activeModules.contains(module)&&!doneModules.contains(module)){
                nonActiveModules.add(module);
            }
        }

        ModuleDTO rdyModuleDTO = new ModuleDTO(activeModules,doneModules, nonActiveModules);

        return rdyModuleDTO;
    }

    @GetMapping("/api/module/{id}")
    public Module getModule(@PathVariable Long id) {
        Module module = moduleRepository.findById(id).get();
        return module;
    }
    @GetMapping("/api/done/module/{id}")
    public void setTopicDone(@PathVariable Long id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
        User newUser = userRepository.findByUsernameIgnoreCase(user.getUsername()).get();
        Topic topic = topicRepository.findById(id).get();

        if(!newUser.getTopicsDone().contains(topic)) {
            List newDoneTopicList = newUser.getTopicsDone();
            newDoneTopicList.add(topic);
            newUser.setTopicsDone(newDoneTopicList);


            List newDoneUserList = topic.getUsersDone();
            newDoneUserList.add(newUser);
            topic.setUsersDone(newDoneUserList);

            userRepository.save(newUser);
            topicRepository.save(topic);
        }

    }


}
