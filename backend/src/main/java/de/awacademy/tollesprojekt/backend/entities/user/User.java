package de.awacademy.tollesprojekt.backend.entities.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.awacademy.tollesprojekt.backend.entities.category.Category;
import de.awacademy.tollesprojekt.backend.entities.topic.Topic;
import de.awacademy.tollesprojekt.backend.entities.module.Module;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
public class User {

    // Fields

    @Id
    @GeneratedValue
    private Long id;

    private String username;

    private String email;

    private String password;

    @JsonIgnore
    @ManyToMany
    @JoinTable (
            name = "user_topic_active",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "topic_id"))
    private List<Topic> topicsActive = new LinkedList<>();

    @JsonIgnore
    @ManyToMany
    @JoinTable (
            name = "user_topic_done",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "topic_id"))
    private List<Topic> topicsDone = new LinkedList<>();

    @JsonIgnore
    @ManyToMany
    @JoinTable (
            name = "user_module_active",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "module_id"))
    List<Module> modulesActive= new LinkedList<>();

    @JsonIgnore
    @ManyToMany
    @JoinTable (
            name = "user_module_done",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "module_id"))
    private List<Module> modulesDone = new LinkedList<>();

    // Constructor

    public User() {
    }

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    // Getter/Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public List<Topic> getTopicsActive() {
        return topicsActive;
    }

    public void setTopicsActive(List<Topic> topicsActive) {
        this.topicsActive = topicsActive;
    }

    public void addTopicActive(Topic topic) {
        topicsActive.add(topic);
    }

    public List<Topic> getTopicsDone() {
        return topicsDone;
    }

    public void setTopicsDone(List<Topic> topicsDone) {
        this.topicsDone = topicsDone;
    }

    public void addTopicDone(Topic topic) {
        topicsDone.add(topic);
    }

    public List<Module> getModulesActive() {
        return modulesActive;
    }

    public void setModulesActive(List<Module> modulesActive) {
        this.modulesActive = modulesActive;
    }

    public void addModuleActive(Module module) {
        modulesActive.add(module);
    }

    public List<Module> getModulesDone() {
        return modulesDone;
    }

    public void setModulesDone(List<Module> modulesDone) {
        this.modulesDone = modulesDone;
    }

    public void addModuleDone(Module module) {
        modulesActive.add(module);
    }

    public List<Topic> getTopicsActiveForCat(Long categoryId) {
        List<Topic> catTopics = new LinkedList<>();
        for (Topic topic : topicsActive) {
            if (categoryId == topic.getCategory().getId()) {
                catTopics.add(topic);
            }
        }
        return catTopics;
    }

    public List<Topic> getTopicsDoneForCat(Long categoryId) {
        List<Topic> catTopics = new LinkedList<>();
        for (Topic topic : topicsDone) {
            if (categoryId == topic.getCategory().getId()) {
                catTopics.add(topic);
            }
        }
        return catTopics;
    }

    public List<Module> getModulesActiveForTopic(Long topicId) {
        List<Module> topModules = new LinkedList<>();
        for (Module module : modulesActive) {
            if (topicId == module.getTopic().getId()) {
                topModules.add(module);
            }
        }
        return topModules;
    }

    public List<Module> getModulesDoneForTopic(Long topicId) {
        List<Module> topModules = new LinkedList<>();
        for (Module topic : modulesDone) {
            if (topicId == topic.getTopic().getId()) {
                topModules.add(topic);
            }
        }
        return topModules;
    }
}
