package de.awacademy.tollesprojekt.backend.security;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {
    @GetMapping("/api/sessionUser")
    public LoginDTO sessionUser(@AuthenticationPrincipal UserDetails userDetails) {
        try {
            return new LoginDTO(userDetails.getUsername(),userDetails.getPassword());
        }
        catch (NullPointerException e) {
            e.getStackTrace();
        }
        return null;
    }
}
